# Sauce Connect Docker

## Description
This is a Docker image with Sauce Connect installed to run in a runner before a testing job executes.

## Resources
* (Sauce Connect Documentation)[https://wiki.saucelabs.com/display/DOCS/Sauce+Connect+Proxy]
* (Sauce Connect Command-Line)[https://wiki.saucelabs.com/display/DOCS/Sauce+Connect+Proxy+Command-Line+Quick+Reference+Guide]
* (Sauce Labs)[https://wiki.saucelabs.com/display/DOCS/Getting+Started]

## connect.sh file
The _connect.sh_ file will require three environment variables to be set:
* SAUCE_TUNNEL_ID - Name of the Tunnel
* SAUCE_USERNAME - Sauce Labs username
* SAUCE_API_KEY - Sauce Account API Key (See Sauce Labs documentation)

## Set CI variables
* CI_REGISTRY_IMAGE - Image Name

## Example build command
### Build
````bash
docker build -t $CI_REGISTRY_IMAGE .  
````
### Run
````bash
&& docker run \
--env SAUCE_USERNAME=$SAUCE_USERNAME \
--env SAUCE_TUNNEL_ID=$SAUCE_TUNNEL_ID \
--env SAUCE_API_KEY=$SAUCE_API_KEY \
-t -i $CI_REGISTRY_IMAGE
````

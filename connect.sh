#!/usr/bin/env bash

# See Sauce Connect Documenation for command line -
# https://wiki.saucelabs.com/display/DOCS/Sauce+Connect+Proxy+Command-Line+Quick+Reference+Guide
sc -u ${SAUCE_USERNAME} -k ${SAUCE_API_KEY} -i ${SAUCE_TUNNEL_ID} -t".naic.org" -P"4445" -s
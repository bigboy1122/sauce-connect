FROM debian:latest
LABEL author='Tony Galati'
ENV SAUCE_VERSION 4.6.2
WORKDIR /usr/local/sauce-connect
RUN apt-get update -qqy && apt-get install -qqy wget
RUN wget https://saucelabs.com/downloads/sc-$SAUCE_VERSION-linux.tar.gz -O - | tar -xz
RUN mv /usr/local/sauce-connect/sc-$SAUCE_VERSION-linux /usr/local/sauce-connect/sc
RUN chmod 777 -R /usr/local/sauce-connect
ENV PATH=/usr/local/sauce-connect/sc/bin:$PATH
ADD connect.sh ./
ENTRYPOINT ["sh","connect.sh"]
EXPOSE 4445
EXPOSE 8032